兩組探險者完成任務歸還，想要聽他們旅行見聞的人再加上想要聽灰狼的事情的人的加入，食堂裡的展現出了不亞於昨日的盛況。旅館的女孩子們圍著圍裙在桌與桌的間隙奔跑著，在水槽與灶台那一方面，以席娜為首的料理負責人也在忙碌著。

在那個更後面一些的位置，粗率的將東西整理還的麻里子和米蘭達二人，正將最後的工作台由庭院向廚房裡搬運。二人共同將其放回了最開始的位置上，就這樣終於恢復了原樣。

「呼。要是這樣的事經常有的話，還真想在井邊要一個帶屋頂的工作場呢。如果會下雨感到為難。這要是下雨的話就麻煩了」
「確定是那樣呢。向塔莉婭大人試著申請一下怎麼樣？　只要麻里子殿說的話很容易就能通過的吧？」

放下了沉重的工作台的二人一邊轉動著肩膀一邊說著閑話的時候，餘光發現了正接近這裡的薩妮婭。

「回來的正好。麻里子桑，雖然說剛回來就這麼急急忙忙的不太好，不過那張桌子可以拜託你嗎？」

在櫃台的一角，滿載著盛滿料理的盤子以及啤酒的托盤的後面，薩妮婭的手指所指的離櫃台稍遠一點的位置，那是五個男人所圍著的一張桌子。

「誒都，那個座位⋯⋯ 誒，貓耳！？　那麼，那個是⋯⋯」

注意到了桌子上的都是誰的麻里子回過頭看向旁邊的米蘭達。察覺到麻里子目光的米蘭達以一臉苦澀表情的點了點頭，然後又明白了麻里子想要說的話而搖了搖頭。

「由我去可以嗎？」
「我去的話又會再騷動起來的。麻里子殿不是喜歡那樣的東西嗎？」
「我不是⋯⋯」

要是說麻里子完全沒有興趣那是假的，但特意這樣來誘導想來就有點惡趣味了。薩妮婭以一臉看到有趣東西的表情看著二人。那表情就和母親一模一樣。

「那麼，我過去了」
「拜託你了」
「交給你了」

穿過櫃台的門，薩妮婭她們的聲音從麻里子背後傳了過來，托起置於櫃台上的托盤。就這樣向那五人的座位走去。

（噢噢，貓耳五人組。男人的貓耳與其說是可愛，不如說是一種幻想啊。不，但是這麼噗魯噗魯地動起來還是很可愛的嘛）

剛從浴室出來的男人們，和穿著工作服的麻里子買了便裝一樣也穿著便服。五人的毛色，或者應該說是髮色嗎，分別是茶色、像暹羅貓那樣耳朵是黑色的白色，深棕，灰色，白色和茶色的混雙色。麻里子一邊按捺住嘴角一邊接近過去。

「讓您久等了」
「哦，來了來了」
「喂，把地方空開」

來到桌子邊的麻里子一出聲，那五個人就吵吵鬧鬧的將桌子上的餐具收拾開來，麻里子在那裡放下了托盤。

「啤酒啤酒」
「啊，那是俺的肉！」

緊接著手伸了過去，托盤上一下子空了。大家都只注意著自己的目標，誰都沒看麻里子一眼。

「收一下空盤子」

在這期間，麻里子再次出聲並探出身子。手伸出去，回收了空掉的盤子以及杯子，而剛剛還很吵鬧那伙人不知為何突然一齊安靜了下來。

「⋯⋯好大」

傳入耳朵的嘟囔聲，讓麻里子注意到不知什麼時候引起了人的關注。為什麼，一瞬間的考慮，馬上就想到了理由。

直立的身體為了收取餐具而傾斜。受到重力的牽引，在直立的時候就向前凸出的圍裙下的胸口，配合著麻里子的活動而搖晃起來。這對於在座的各位男性來說，正好是在眼前高度出現的晃動。沒可能不進入視線裡。

（年輕啊）

多少是有些害羞，但比起那個佔據麻里子內心的則大部分都是對此表示理解。因為麻里子也一直注視著貓耳和貓尾，雙方也都彼此彼此。麻里子緩緩的直立起身子，咳哼地咳了一下。

除去茶色頭髮以外的四名男性身軀一震。大張著嘴的人驚慌的將嘴合上，停止在這個座位上的時間再次開始流動起來。男人們抬起頭看著第一次見面的麻里子。

「那個？　是第一次見面的人」

以混色頭男人的聲音為開端，其他人也開始了說話。

「是的。承蒙您的關照，是從前天開始剛來這裡的。我是麻里子」

麻里子浮起輕輕地微笑──營業用──並低下頭，男人們紛紛開口。

「是 伊戈爾。二十歳，單身」
「叫烏戈。十九。我也是單身」
「埃貢，十八歳，一個人」
「是歐貝德。到十八歳了。戀人募集中」

按著暹羅，深棕，灰色，混色的順序。因為從伊戈爾開始，全員都強調著年齡和單身，這讓麻里子差點笑了出來。而且四個人在看過麻里子的臉後可以明顯感受到他們準確落到胸部上的視線。

（以前就聽說過作為被看的一方可以感受到視線，這還真是感受的清楚明白呢）

「我名為阿德雷。年紀二十歳。大致上，是由我來擔任這個隊伍的隊長。以後、還請多關照」
「彼此彼此，請多關照」

最後茶色頭髮的男人開口說道。這稍稍有些戲劇的語調讓麻里子有點驚訝。

（噢噢，這個人就是米蘭達說的⋯⋯叫什麼好呢，追隨者？　認真說的話還蠻正經的嘛？　只有這個人沒看胸）

就在麻里子一邊打著招呼一邊觀察著阿德雷的時候，在稍微遠離一些的席位響起了嘎登地一聲不知什麼東西倒下的聲音。
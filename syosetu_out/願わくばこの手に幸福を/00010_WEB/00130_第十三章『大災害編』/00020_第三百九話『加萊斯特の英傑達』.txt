「悲哀和災難，一旦暴露了自己的弱點，這些東西就開始一個個找上門來。真是麻煩啊。」

卡拉伊斯特王国高位貴族，羅伊梅茲・福莫爾用與那個巨大的身體相反，令人感到纖細和才智的聲音說著。其大大的瞳孔背後，似乎隱藏著幾重疑惑。
本來應該由十數名以上的高官貴族和聖職者們充斥著的王国圓桌議場。但是今天這裡只有坐在座位上的兩個男人。除此外連一個護衛的身影都看不到，寂靜得離奇。

羅伊梅茨・福莫爾坐在下座。而在其上座的，是一個身材纖細，手指相當長的男人。
叼著香煙，仿彿看向遠方的雙眼之下，是很深的臉頰，穿著黑色軍裝和紅色外套。這個男人沉重地說道。

「在肥沃的土壤中才會雜草叢生，福莫爾卿。」

卡拉伊斯特王国擁有廣闊的版圖。男子接著說道，既然如此，發生一兩個悲劇應該才正常吧。
然後，他張開薄薄的嘴唇繼續話語。像這樣反覆說話對於這個男人來說是很少見的事情。
這原因與其說是他的性格慎重或者政敵太多，還不如說他只是單純地不擅長說話而已。

「我知道你想說的話。是魔獸群會增加這種威脅吧」

羅伊梅茲的巨大身軀誇張地點頭，表示肯定。他的每一個動作無不蘊含著奇妙的氣魄。表情有些無法去除的壓抑。

在這個地方，最近所有上升到王国層面的話題全都帶上了陰影。唯一能算得上開朗的話題，最近大概也就是聖女候選人終於從大聖堂中脫穎而出吧。

除了那個開朗的話題之外，其餘都是些麻煩。從城牆都市伽羅亞瑪利亞的失陷開始，到薩尼奧會戰中最終敗北。卡拉伊斯特王国旗下的貝爾菲因、菲洛絲這兩個自治城市也在這場福音戰爭中相繼淪陷。

接著，又有兩件麻煩事拖著腳步來到了王国。
一件是，聖女在巡禮中受到了舊教徒的大規模襲擊。由於襲擊，弗里姆斯拉特的大神殿在雪中坍塌，馳名全国的聖堂騎士也有多人負傷。

幸運的是，聖女阿琉爾娜平安無事。另外，由於聖堂騎士加爾拉斯・加爾岡蒂亞和同行者赫爾因・斯坦利的努力而最終抑制了受害的程度，但原本聖女的朝聖被阻礙這樣的事，應該是不可能發生的。
這樣的醜聞自然不能傳入市民的耳中，對應的大教堂有一部分機能也無法運轉了。

還有一件麻煩事，就是在死雪中開始活躍的魔獸們。

「來自西北的魔獸群很有氣勢地進軍（原話是不知道在哪裡會失去氣勢，意譯為這個）。這才是史無前例的。」

羅伊梅茨重複了負責防御西北城寨的瓦蕾麗・布萊特尼斯的報告。雖然報告裡沒有直接提到上面的話，但是這個男人也十分理解了其中的含義。
最後，為了對抗魔獸群，希望軍隊能增派戰力。

這是理所當然的事情，但是在卡拉伊斯特王国，這個行動卻不能輕易進行。
這個擁有強大身體和獠牙的国家，隨著體量的上升而變得遲鈍了，它的頭腦也同樣肥大化了。因為這樣，這個国家終於到了不能隨意動彈身體的地步。
形式上，卡拉伊斯特王国是国王阿梅萊茨・卡拉伊斯特一人在上的君主政治。不過，實際的情況甚至比糾結的線還要複雜。
由貴族構成的政機院的影響力像毒藥一樣滲透到国家的方方面面，而大教堂的勢力也在蠢蠢欲動（原文為眼睛在四處張望，意譯為此）

而且，被尊為治世王的現任国王已經老邁，不復當年的強盛，因此政治的步伐當然會變得緩慢。
如果遵循正式的程序，僅僅只是通過一個政策就需要花費極為漫長的時間，這情景已經常態化了。

雖說如此，但這樣的情況如果僅是發生在政治領域，應該還不可能產生大規模的問題。雖然運轉上發生了故障，但另一方面也可以說有助於作出慎重的判斷吧。

但是，如果在軍事領域就不行了。
一瞬間的遲疑就足以殺死士兵，損壊国家的脊梁了。正因為如此，與政治分離的軍隊的調度方面，存在著一位被稱為国王直屬下屬的獨裁權限者。
被給予那個獨裁權限的，就是這個細長的男人。保護国家之人，護国官杰伊斯・布萊克貝利。
在這漫長的時代裡，在卡拉伊斯特王国，所謂護国官指的就是他。

與羅伊梅茲相比，可說貧弱的雙肩，卻被壓上了卡拉伊斯特王国軍隊的全部指揮權的沉重壓力。到底這份擔子的重量有多大，恐怕只有布萊克貝利本人才知道。

布萊克貝利吐著香煙的煙氣說：

「陛下已經聽聞了。但是不行了，陛下已經無法做出判斷了。」

布萊克貝利所宣告的言語中沒有焦躁或之類的東西，似乎只是在述說事實而已。

雖然有批判国王的話語，但羅伊梅茨也無動於衷，只是表示已經理解了，微微點了點頭。

「正因為如此，我才來到這裡，布萊克貝利護国官。」

羅伊梅茨的話語中包含著很多內容，凝視著布萊克貝利。那個大大的瞳孔顯得比平時更為堅定。
沒有道理。就算以羅伊梅茨的身份和實力，現在其正在進行的事情也像是在破碎的薄冰上行走一樣。
不管怎麼說，雖說是高位貴族，但本來是不能直接向護国官提出請求的。因為護国官是應該與政治完全分離的存在。
如果這件事暴露給羅伊梅茲的政敵，他們一定會像蝗虫一樣咬住這點不放的。
但是，羅伊梅茲有這樣冒険的意義。而且，布萊克貝利也是如此。

布萊克貝利再次用嘴唇叼著煙，一瞬間像是在思考什麼似的，眨著睫毛。其大大的白眼仁中浮現的蒼色，顯得格外突出。
在煙草製造的搖曳上升的白煙中，布萊克貝利說。

「對於從事政治工作的人來說，你真是個坦率的男人啊。」
「嗯。坦率是政治上最重要的東西。」

是嗎，布萊克貝利點了點頭。這一句話似乎決定了什麼。他纖細的身體站起來，一邊搖晃著紅色的外套，一邊繼續著話語。

深黑的眼睛，帶著異樣的重壓打開著。

「和加爾拉斯・加爾岡蒂亞是大教堂的英傑一樣，瓦蕾麗・布萊特尼斯一定也是王国中無法失去的英傑。我也覺得因為凡人的愚蠢行為而導致她的死亡的話，那是多麼愚蠢的事情。」

這句話意味著羅伊梅茲的話語被大部分接受了。為此羅伊梅茲不由得在胸中吐出了放心的呼吸。

並不是不相信瓦蕾麗。即使魔獸群像云霞一樣蜂擁而至，但羅伊梅茲確信只要是有她的魔術鎧甲的話，就能將這一切擊退扭轉。
但是，如果要比喻的話，站在戰場的人總是背負著一個惡魔的。所謂戰場，就是那樣的東西。
羅伊梅茨確信正因為如此，未站在戰場的人應該背負相應的義務。向自己發誓忠誠的人已經站在戰場上。既然如此，主人就必須給予最大限度的支援。
正因為如此，主人才能得到對部下的指揮權，而部下則向主人獻上劍。這種關係如果崩潰，那麼主從的情感就不可能存在了吧。
當然，在理查德受了傷無法動彈的今天，羅伊梅茨心中也存在著瓦蕾麗是個無法失去的人才這樣理所當然的計算。所謂感情與計算，無論何時都是能左右天平的。

布萊克貝利熄滅了香煙，淡淡地宣告。似乎在他的腦海中，相關的計劃已經做完了。

「根據她現在擁有的權限，因此可以增援的兵數不會如想像中多。所以暫時讓她回到王都，這樣也不錯啊，福莫爾卿。」

羅伊梅茲像往常一樣，用力地移動著巨大的軀體點了點頭。
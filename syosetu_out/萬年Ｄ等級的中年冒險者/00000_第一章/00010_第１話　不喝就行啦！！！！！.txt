「喂，大叔，你在幹什麼？」
「⋯⋯唉，果然還是想快點回去啊──」
「嘖，連撿素材這麼低級簡單的工作都做不好嗎？真是沒用啊。」

可惡，真煩啊小鬼們，你們就不能來幫一幫我忙？

我一邊在心裡咒罵著，一邊拚命地收集著散落在附近周圍的素材。

反正這是被捨棄的低級工作，所以做這個並不能賺到多少錢，
但只要做多還是能夠賺到一些錢的。

包括我在內的合計４人的冒険者隊伍，這次消滅了十幾隻以上的狗頭人。
按成果４人平分的話，每個人大概能拿到約２～３枚銀幣。

⋯⋯反正我那份充其量不過也就是一半以下。

「諾，這裡也掉了。」

隊伍的隊長，體格較好的劍士雷克指著自己腳邊的素材說道，
雖然我認為他自己也能撿得到，但我還是把左手伸向了素材────啊？！！好痛！！！

「喔，不好意思，稍微腳滑了一下，痛嗎？」

被雷克給踩中了手。
抬頭一看，那傢伙居然擺出一副嘲笑的嘴臉俯視著我。

這個死小鬼⋯⋯不管怎麼想、他肯定是故意的。
因為很不爽，所以不禁瞪了他一眼。

「啊？你有什麼意見想說的嗎？」

於是就乾脆將錯就錯的忍了下來。
雖然說也不是不想反抗一點之類的，但是這麼做只會讓我的報酬變得更少。

「不⋯⋯沒什麼⋯⋯」

我只能勉強抑制住怒氣，把眼神從他身上移開乾巴巴地回答道。

「哈哈哈，真是可憐吶。我絶對不要變成像你這樣悲慘的大叔。」

看到了這些，盜賊職的薩魯賈大笑了起來。

「啊，這麼說起來，這個大叔，好像偶爾會色眯眯地盯著我看吶？」

隊伍裡唯一的魔法師，瑪麗哼了一聲。

（插圖００１－００５）

「真的假的⋯⋯你這傢伙，居然敢對別人的女人有非分之想啊？」

雷克就這樣把手搭上了瑪麗的肩膀、一把拉了過來抱著，向我忠告道。

他們兩人正在交往。

「這傢伙，都這個年紀了都還是單身吧？哈哈哈！
絶對是想著瑪麗的事，每天晚上都在做些什麼吧！」

「喂！別說了！我光是想像了一下，就想吐了啊～～～」

⋯⋯很抱歉我對你這種性質惡劣的婊子不感興趣。

不過確實，瑪麗的外表不錯，身材也是男人的理想的體型。
因此，有時會偶爾偷瞄一眼她的胸部和臀部，但沒有到死死的盯著看的程度。

所以薩魯賈說的那種事情也──不，好像有過一兩次的樣子⋯⋯⋯

如果我沒記錯的話，雷克二十二歳，瑪麗二十歳，薩魯賈是二十一歳。
另一邊，我的年齡是三十七歳。

⋯⋯雖然外表看上去好似快要四十五歳的樣子，不過的的確確是三十七歳。

所以被都可以當作自己孩子年紀的年輕人當成笨蛋般恥笑，怎麼可能會甘心。

───

原本這個隊伍是由雷克他們三人組成，但因為被邀請後來加入了。
那已經是半年前的事了。

就這麼離開隊伍──我這麼想過好幾次了。
儘管如此還是和他們繼續冒険的理由十分簡單，
確實一次賺到的錢很少，但只要還在這個隊伍裡面，就能夠賺到錢。

十八歳的時候成為冒険者，已經快二十年了。
也能算得上是資深老手了。
然而，成為冒険者之後只有經歷過一次晉級，之後就是萬年Ｄ等級的底層冒険者。

新人時期的時候還曾備受期待。
但是，在十九歳的時候拜魔物所賜，右手不能夠使用了。

但是儘管如此，在年輕的時候還是能繼續做下去。
然而，在這十幾年的歳月裡，因為受到了各種傷，身體已經破爛不堪了。
因此，每年工作賺的錢越來越少。

雖然有試過用回復魔法，但是舊傷是無法靠回復魔法或魔法藥水就能治好的。

特別是這幾年，生活變得異常地困難。
所以被雷克他們給邀請的時候，心裡其實是非常開心的。

但是之後，等待我的是背負行李的每一天，他們從來都沒有期待我能成為戰鬥力過。
對冒険者這個職業付出了大量歳月的我，已經不能靠其他工作生活下去。
他們或許是意識到了我那軟弱的處境，才把我當做奴隷般的使喚。

───

這一天，我的報酬是銀幣三枚。
如果不拿去奢侈一下的話，這就相當於兩天左右的生活費了。

考慮到我自己一個人最多隻能打倒４隻魔物，而且比起單獨行動死亡率更低、
又能夠抑制自己經費的使用，那也算是賺到了。
如果我從以前開始每天就存點錢的話，也許現在也不會這麼淒涼。

但我不想存錢
大部分的錢都拿去買酒喝了。

我在冒険完回去的時候，幾乎、十有八九的會去喝酒，去喝那些便宜的麥芽啤酒。
說實話不怎麼好喝，而且我也不想要多麼猛烈的飲酒，但也需要發泄壓力。

另外便宜也沒有關係，只要能喝醉就足夠了。
只有在喝醉後，我的心靈才能夠收到一絲絲的安慰。

「喂，盧卡斯，今天比起平時，喝的太多了啊。」
「不喝就不行啊！」

老闆擔心著喝醉酒的我。
雖然有點遲了，但盧卡斯是我的名字。
我一口氣幹完了杯中剩下的酒。

「再來一杯！」
「算了吧，而且馬上也要關門了。」
「不要這麼小氣嘛，只要還有顧客就別關門～」
「別鬼扯！」

結果，被強行地驅逐出了酒吧。

「回去的路上小心點哦」
「好好～」

我向店長揮手，然後腳步蹣跚地走了起來。

「咦？這裡是什麼地方？」

回過神來的時候，發現了自己來到了一個陌生的地方。

不對，我已經在這條街上呆上了數十年，
沒有我不知道的地方。

仔細觀察後，發現這裡是城市中心的廣場。
但是是和我所居住的家的方向是相反的。
好像是走錯路了。

不過也好，稍微吹來了一絲夜風，有了想乘涼的心情。

───

我來到了廣場的中心。

那裡有著一座巨大的岩石，還有一把插在岩石上方的直劍。

那是傳說中的英雄所使用的────

「──穿說、只劍」

發音因為喝醉了好像怪怪的。

據說，那是傳說中的英雄所使用的劍。
好像是在這座城市出現之前的事了吧
也就是說，大概兩、三百年前，那把劍就已經這樣插進了那個地方。

雖然看起來是一把非常普通的直劍，

但是經歷了幾百年歳月的洗禮，卻沒有生鏽變鈍什麼的。

過去人們曾試圖使用各種手段想要拔出那把劍。
但是全部都以失敗告終。
而且那個插著劍的岩石本身也是用的特殊的礦物所製成的，好像也無法破壊和轉移。

平常的話，在白天看到些前來挑戰的人

但現在因為很晚了，所以沒有一個人影都沒見著。

「英—雄—麼」

腦海裏浮現出了小時候的夢想，
鄉下農村出身的我，十分憧憬著英雄。

成為侍奉王宮的騎士，提升業績後成為近衛軍。

但有一天，神話中才會出現的邪惡之龍出現了，然後搶走了公主。
之後，和同伴一起來到惡龍的面前，並擊敗它。

成功救出公主後凱旋而歸，成為名副其實的英雄人物──

真的是像小孩子般天真的夢想呢。
要是年紀再大上一點的話，就不會有這種痴心妄想了吧。
想去報考王都的騎士培育學校，不顧周圍人的反對，在十五歳的時候跑出了村子。
雖然已經挑戰三次了，但最終還是沒能考上。

之後不得已成為了冒険者⋯⋯然後就是現在。

⋯⋯此時此刻，我因為喝酒而陷入了醉酒的狀態。

趁著喝醉酒的趨勢，感覺現在的自己什麼事都能做得出來。
明明是沒辦法筆直前行的狀態，回過神來，我卻發現自己已經登上了岩石。

「本大爺可是英—熊—啊～。不可能拔不出這把劍的呀！」

我五音不全地大聲亂叫。
如果沒喝醉的話，才不會到了這個年紀還做這麼丟臉的事。

儘管如此，但現在的我是醉酒的狀態。

雖然沒有任何的依據，但現在的我卻充滿了不管做什麼感覺都能成功的自信。

對著面前插在岩石中的直劍，我用左手握住劍柄，猛的一用力將它拉到頭上。

嘶嚓。

──拔出來了。

「哈哈哈！怎麼樣～！看到了吧～！穿說只劍拔出────誒？！！！」
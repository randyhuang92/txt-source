# CONTENTS

萬年Ｄ等級的中年冒險者  
万年Ｄランクの中年冒険者、酔った勢いで伝説の剣を引っこ抜く  
萬年D等級的中年冒險者、藉著酒勢拔出了傳說之劍  

作者： 九頭七尾  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- :pencil: [整合樣式](%E6%95%B4%E5%90%88%E6%A8%A3%E5%BC%8F.md)
- [含有原文的章節](ja.md) - 可能為未翻譯或者吞樓，等待圖轉文之類
- [待修正屏蔽字](%E5%BE%85%E4%BF%AE%E6%AD%A3%E5%B1%8F%E8%94%BD%E5%AD%97.md) - 需要有人協助將 `**` 內的字補上
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E8%90%AC%E5%B9%B4%EF%BC%A4%E7%AD%89%E7%B4%9A%E7%9A%84%E4%B8%AD%E5%B9%B4%E5%86%92%E9%9A%AA%E8%80%85.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/%E8%90%AC%E5%B9%B4D%E7%AD%89%E7%B4%9A%E7%9A%84%E4%B8%AD%E5%B9%B4%E5%86%92%E9%9A%AA%E8%80%85%E3%80%81%E8%97%89%E8%91%97%E9%85%92%E5%8B%A2%E6%8B%94%E5%87%BA%E4%BA%86%E5%82%B3%E8%AA%AA%E4%B9%8B%E5%8A%8D.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/out/%E8%90%AC%E5%B9%B4D%E7%AD%89%E7%B4%9A%E7%9A%84%E4%B8%AD%E5%B9%B4%E5%86%92%E9%9A%AA%E8%80%85%E3%80%81%E8%97%89%E8%91%97%E9%85%92%E5%8B%A2%E6%8B%94%E5%87%BA%E4%BA%86%E5%82%B3.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/syosetu_out/萬年Ｄ等級的中年冒險者/導航目錄.md "導航目錄")




## [第一章](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0)

- [第１話　不喝就行啦！！！！！](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00010_%E7%AC%AC%EF%BC%91%E8%A9%B1%E3%80%80%E4%B8%8D%E5%96%9D%E5%B0%B1%E8%A1%8C%E5%95%A6%EF%BC%81%EF%BC%81%EF%BC%81%EF%BC%81%EF%BC%81.txt)
- [第２話　先去洗臉吧](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00020_%E7%AC%AC%EF%BC%92%E8%A9%B1%E3%80%80%E5%85%88%E5%8E%BB%E6%B4%97%E8%87%89%E5%90%A7.txt)
- [第３話　這傢伙，到底在胡些說什麼啊？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00030_%E7%AC%AC%EF%BC%93%E8%A9%B1%E3%80%80%E9%80%99%E5%82%A2%E4%BC%99%EF%BC%8C%E5%88%B0%E5%BA%95%E5%9C%A8%E8%83%A1%E4%BA%9B%E8%AA%AA%E4%BB%80%E9%BA%BC%E5%95%8A%EF%BC%9F.txt)
- [第４話　這傢伙真的是神劍嗎......？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00040_%E7%AC%AC%EF%BC%94%E8%A9%B1%E3%80%80%E9%80%99%E5%82%A2%E4%BC%99%E7%9C%9F%E7%9A%84%E6%98%AF%E7%A5%9E%E5%8A%8D%E5%97%8E......%EF%BC%9F.txt)
- [第５話　這是什麼懲罰遊戲？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00050_%E7%AC%AC%EF%BC%95%E8%A9%B1%E3%80%80%E9%80%99%E6%98%AF%E4%BB%80%E9%BA%BC%E6%87%B2%E7%BD%B0%E9%81%8A%E6%88%B2%EF%BC%9F.txt)
- [第６話　這不就只是色老頭嗎？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00060_%E7%AC%AC%EF%BC%96%E8%A9%B1%E3%80%80%E9%80%99%E4%B8%8D%E5%B0%B1%E5%8F%AA%E6%98%AF%E8%89%B2%E8%80%81%E9%A0%AD%E5%97%8E%EF%BC%9F.txt)
- [第７話　股間也是一樣吶～](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00070_%E7%AC%AC%EF%BC%97%E8%A9%B1%E3%80%80%E8%82%A1%E9%96%93%E4%B9%9F%E6%98%AF%E4%B8%80%E6%A8%A3%E5%90%B6%EF%BD%9E.txt)
- [第８話　幸運的色狼～](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00080_%E7%AC%AC%EF%BC%98%E8%A9%B1%E3%80%80%E5%B9%B8%E9%81%8B%E7%9A%84%E8%89%B2%E7%8B%BC%EF%BD%9E.txt)
- [第９話　這已經算是跟蹤狂了吧？！](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00090_%E7%AC%AC%EF%BC%99%E8%A9%B1%E3%80%80%E9%80%99%E5%B7%B2%E7%B6%93%E7%AE%97%E6%98%AF%E8%B7%9F%E8%B9%A4%E7%8B%82%E4%BA%86%E5%90%A7%EF%BC%9F%EF%BC%81.txt)
- [第10話　真不巧現在是賢者模式啊！](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00100_%E7%AC%AC10%E8%A9%B1%E3%80%80%E7%9C%9F%E4%B8%8D%E5%B7%A7%E7%8F%BE%E5%9C%A8%E6%98%AF%E8%B3%A2%E8%80%85%E6%A8%A1%E5%BC%8F%E5%95%8A%EF%BC%81.txt)
- [第11話　並沒有和你親近過的記憶？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00110_%E7%AC%AC11%E8%A9%B1%E3%80%80%E4%B8%A6%E6%B2%92%E6%9C%89%E5%92%8C%E4%BD%A0%E8%A6%AA%E8%BF%91%E9%81%8E%E7%9A%84%E8%A8%98%E6%86%B6%EF%BC%9F.txt)
- [第12話　所以說我不是變態啦！](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00120_%E7%AC%AC12%E8%A9%B1%E3%80%80%E6%89%80%E4%BB%A5%E8%AA%AA%E6%88%91%E4%B8%8D%E6%98%AF%E8%AE%8A%E6%85%8B%E5%95%A6%EF%BC%81.txt)
- [第13話　到最後可能會比死還要痛苦](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00130_%E7%AC%AC13%E8%A9%B1%E3%80%80%E5%88%B0%E6%9C%80%E5%BE%8C%E5%8F%AF%E8%83%BD%E6%9C%83%E6%AF%94%E6%AD%BB%E9%82%84%E8%A6%81%E7%97%9B%E8%8B%A6.txt)
- [第14話　把它敲斷一次試試吧？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00140_%E7%AC%AC14%E8%A9%B1%E3%80%80%E6%8A%8A%E5%AE%83%E6%95%B2%E6%96%B7%E4%B8%80%E6%AC%A1%E8%A9%A6%E8%A9%A6%E5%90%A7%EF%BC%9F.txt)
- [第15話　把他菊花的洞給捅爛！](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00150_%E7%AC%AC15%E8%A9%B1%E3%80%80%E6%8A%8A%E4%BB%96%E8%8F%8A%E8%8A%B1%E7%9A%84%E6%B4%9E%E7%B5%A6%E6%8D%85%E7%88%9B%EF%BC%81.txt)
- [第16話　只是姆啾～的一下就好啦！](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00160_%E7%AC%AC16%E8%A9%B1%E3%80%80%E5%8F%AA%E6%98%AF%E5%A7%86%E5%95%BE%EF%BD%9E%E7%9A%84%E4%B8%80%E4%B8%8B%E5%B0%B1%E5%A5%BD%E5%95%A6%EF%BC%81.txt)
- [第17話　那麼，一起睡吧。](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00170_%E7%AC%AC17%E8%A9%B1%E3%80%80%E9%82%A3%E9%BA%BC%EF%BC%8C%E4%B8%80%E8%B5%B7%E7%9D%A1%E5%90%A7%E3%80%82.txt)
- [第18話　難道奧克對我有什麼怨念嗎？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00180_%E7%AC%AC18%E8%A9%B1%E3%80%80%E9%9B%A3%E9%81%93%E5%A5%A7%E5%85%8B%E5%B0%8D%E6%88%91%E6%9C%89%E4%BB%80%E9%BA%BC%E6%80%A8%E5%BF%B5%E5%97%8E%EF%BC%9F.txt)
- [第19話　你是認真的嗎？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00190_%E7%AC%AC19%E8%A9%B1%E3%80%80%E4%BD%A0%E6%98%AF%E8%AA%8D%E7%9C%9F%E7%9A%84%E5%97%8E%EF%BC%9F.txt)
- [第20話　有時會和劍說話](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00200_%E7%AC%AC20%E8%A9%B1%E3%80%80%E6%9C%89%E6%99%82%E6%9C%83%E5%92%8C%E5%8A%8D%E8%AA%AA%E8%A9%B1.txt)
- [第21話　你的自我評價太低了吧？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00210_%E7%AC%AC21%E8%A9%B1%E3%80%80%E4%BD%A0%E7%9A%84%E8%87%AA%E6%88%91%E8%A9%95%E5%83%B9%E5%A4%AA%E4%BD%8E%E4%BA%86%E5%90%A7%EF%BC%9F.txt)
- [第22話　能抱我嗎？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00220_%E7%AC%AC22%E8%A9%B1%E3%80%80%E8%83%BD%E6%8A%B1%E6%88%91%E5%97%8E%EF%BC%9F.txt)
- [第23話　要好好地負起責任來吶。](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00230_%E7%AC%AC23%E8%A9%B1%E3%80%80%E8%A6%81%E5%A5%BD%E5%A5%BD%E5%9C%B0%E8%B2%A0%E8%B5%B7%E8%B2%AC%E4%BB%BB%E4%BE%86%E5%90%B6%E3%80%82.txt)
- [第24話　不，這是真的！](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00240_%E7%AC%AC24%E8%A9%B1%E3%80%80%E4%B8%8D%EF%BC%8C%E9%80%99%E6%98%AF%E7%9C%9F%E7%9A%84%EF%BC%81.txt)
- [第25話　只要睡一會就好了](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00250_%E7%AC%AC25%E8%A9%B1%E3%80%80%E5%8F%AA%E8%A6%81%E7%9D%A1%E4%B8%80%E6%9C%83%E5%B0%B1%E5%A5%BD%E4%BA%86.txt)
- [第26話　因為這是愛的結晶吶！](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00260_%E7%AC%AC26%E8%A9%B1%E3%80%80%E5%9B%A0%E7%82%BA%E9%80%99%E6%98%AF%E6%84%9B%E7%9A%84%E7%B5%90%E6%99%B6%E5%90%B6%EF%BC%81.txt)
- [第27話　因為我是那位大人的女人。](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00270_%E7%AC%AC27%E8%A9%B1%E3%80%80%E5%9B%A0%E7%82%BA%E6%88%91%E6%98%AF%E9%82%A3%E4%BD%8D%E5%A4%A7%E4%BA%BA%E7%9A%84%E5%A5%B3%E4%BA%BA%E3%80%82.txt)
- [第28話　真是個讓人不爽的混蛋](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00280_%E7%AC%AC28%E8%A9%B1%E3%80%80%E7%9C%9F%E6%98%AF%E5%80%8B%E8%AE%93%E4%BA%BA%E4%B8%8D%E7%88%BD%E7%9A%84%E6%B7%B7%E8%9B%8B.txt)
- [第29話　請別逗我笑好嗎？](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00290_%E7%AC%AC29%E8%A9%B1%E3%80%80%E8%AB%8B%E5%88%A5%E9%80%97%E6%88%91%E7%AC%91%E5%A5%BD%E5%97%8E%EF%BC%9F.txt)
- [第30話　已經是個優秀的大人了啊。](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00300_%E7%AC%AC30%E8%A9%B1%E3%80%80%E5%B7%B2%E7%B6%93%E6%98%AF%E5%80%8B%E5%84%AA%E7%A7%80%E7%9A%84%E5%A4%A7%E4%BA%BA%E4%BA%86%E5%95%8A%E3%80%82.txt)


## [第二章](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0)

- [第１話　如果不是那樣子的性格的話就很可愛啊……](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00010_%E7%AC%AC%EF%BC%91%E8%A9%B1%E3%80%80%E5%A6%82%E6%9E%9C%E4%B8%8D%E6%98%AF%E9%82%A3%E6%A8%A3%E5%AD%90%E7%9A%84%E6%80%A7%E6%A0%BC%E7%9A%84%E8%A9%B1%E5%B0%B1%E5%BE%88%E5%8F%AF%E6%84%9B%E5%95%8A%E2%80%A6%E2%80%A6.txt)
- [第２話　對男人的屁股一見鍾情了。](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00020_%E7%AC%AC%EF%BC%92%E8%A9%B1%E3%80%80%E5%B0%8D%E7%94%B7%E4%BA%BA%E7%9A%84%E5%B1%81%E8%82%A1%E4%B8%80%E8%A6%8B%E9%8D%BE%E6%83%85%E4%BA%86%E3%80%82.txt)
- [第３話　佇立在那邊的公主](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00030_%E7%AC%AC%EF%BC%93%E8%A9%B1%E3%80%80%E4%BD%87%E7%AB%8B%E5%9C%A8%E9%82%A3%E9%82%8A%E7%9A%84%E5%85%AC%E4%B8%BB.txt)
- [第４話　我現在變成孤身一人了](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00040_%E7%AC%AC%EF%BC%94%E8%A9%B1%E3%80%80%E6%88%91%E7%8F%BE%E5%9C%A8%E8%AE%8A%E6%88%90%E5%AD%A4%E8%BA%AB%E4%B8%80%E4%BA%BA%E4%BA%86.txt)
- [第５話　總感覺哪裡對我很冷淡](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00050_%E7%AC%AC%EF%BC%95%E8%A9%B1%E3%80%80%E7%B8%BD%E6%84%9F%E8%A6%BA%E5%93%AA%E8%A3%A1%E5%B0%8D%E6%88%91%E5%BE%88%E5%86%B7%E6%B7%A1.txt)
- [第６話　請加入本公主的學生小隊](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00060_%E7%AC%AC%EF%BC%96%E8%A9%B1%E3%80%80%E8%AB%8B%E5%8A%A0%E5%85%A5%E6%9C%AC%E5%85%AC%E4%B8%BB%E7%9A%84%E5%AD%B8%E7%94%9F%E5%B0%8F%E9%9A%8A.txt)
- [第７話　這裡留有珂露雪大人的唾液](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00070_%E7%AC%AC%EF%BC%97%E8%A9%B1%E3%80%80%E9%80%99%E8%A3%A1%E7%95%99%E6%9C%89%E7%8F%82%E9%9C%B2%E9%9B%AA%E5%A4%A7%E4%BA%BA%E7%9A%84%E5%94%BE%E6%B6%B2.txt)
- [第８話　不要再靠近公主大人了](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00080_%E7%AC%AC%EF%BC%98%E8%A9%B1%E3%80%80%E4%B8%8D%E8%A6%81%E5%86%8D%E9%9D%A0%E8%BF%91%E5%85%AC%E4%B8%BB%E5%A4%A7%E4%BA%BA%E4%BA%86.txt)
- [第９話　好感度上升了很多喲](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00090_%E7%AC%AC%EF%BC%99%E8%A9%B1%E3%80%80%E5%A5%BD%E6%84%9F%E5%BA%A6%E4%B8%8A%E5%8D%87%E4%BA%86%E5%BE%88%E5%A4%9A%E5%96%B2.txt)
- [第10話　難不成是搞笑角色](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00100_%E7%AC%AC10%E8%A9%B1%E3%80%80%E9%9B%A3%E4%B8%8D%E6%88%90%E6%98%AF%E6%90%9E%E7%AC%91%E8%A7%92%E8%89%B2.txt)
- [第11話　作為代替](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00110_%E7%AC%AC11%E8%A9%B1%E3%80%80%E4%BD%9C%E7%82%BA%E4%BB%A3%E6%9B%BF.txt)
- [第12話　油光滑潤的嘴唇看起來有點工口呢](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00120_%E7%AC%AC12%E8%A9%B1%E3%80%80%E6%B2%B9%E5%85%89%E6%BB%91%E6%BD%A4%E7%9A%84%E5%98%B4%E5%94%87%E7%9C%8B%E8%B5%B7%E4%BE%86%E6%9C%89%E9%BB%9E%E5%B7%A5%E5%8F%A3%E5%91%A2.txt)
- [第13話　一次看到了男人的那個……](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00130_%E7%AC%AC13%E8%A9%B1%E3%80%80%E4%B8%80%E6%AC%A1%E7%9C%8B%E5%88%B0%E4%BA%86%E7%94%B7%E4%BA%BA%E7%9A%84%E9%82%A3%E5%80%8B%E2%80%A6%E2%80%A6.txt)
- [第14話　但是完全沒事呢？](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00140_%E7%AC%AC14%E8%A9%B1%E3%80%80%E4%BD%86%E6%98%AF%E5%AE%8C%E5%85%A8%E6%B2%92%E4%BA%8B%E5%91%A2%EF%BC%9F.txt)
- [第15話　要是把我扔出去的話，我可饒不了你哦？](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00150_%E7%AC%AC15%E8%A9%B1%E3%80%80%E8%A6%81%E6%98%AF%E6%8A%8A%E6%88%91%E6%89%94%E5%87%BA%E5%8E%BB%E7%9A%84%E8%A9%B1%EF%BC%8C%E6%88%91%E5%8F%AF%E9%A5%92%E4%B8%8D%E4%BA%86%E4%BD%A0%E5%93%A6%EF%BC%9F.txt)
- [第16話　不一定要在這裡做也可以吧](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00160_%E7%AC%AC16%E8%A9%B1%E3%80%80%E4%B8%8D%E4%B8%80%E5%AE%9A%E8%A6%81%E5%9C%A8%E9%80%99%E8%A3%A1%E5%81%9A%E4%B9%9F%E5%8F%AF%E4%BB%A5%E5%90%A7.txt)
- [第17話　反而是我這邊該向你道謝](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00170_%E7%AC%AC17%E8%A9%B1%E3%80%80%E5%8F%8D%E8%80%8C%E6%98%AF%E6%88%91%E9%80%99%E9%82%8A%E8%A9%B2%E5%90%91%E4%BD%A0%E9%81%93%E8%AC%9D.txt)
- [第18話　差不多也該注意到了吧](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00180_%E7%AC%AC18%E8%A9%B1%E3%80%80%E5%B7%AE%E4%B8%8D%E5%A4%9A%E4%B9%9F%E8%A9%B2%E6%B3%A8%E6%84%8F%E5%88%B0%E4%BA%86%E5%90%A7.txt)
- [第19話　沒有長著那邊？](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00190_%E7%AC%AC19%E8%A9%B1%E3%80%80%E6%B2%92%E6%9C%89%E9%95%B7%E8%91%97%E9%82%A3%E9%82%8A%EF%BC%9F.txt)
- [第20話　乾脆先去死一回怎麼樣？](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00200_%E7%AC%AC20%E8%A9%B1%E3%80%80%E4%B9%BE%E8%84%86%E5%85%88%E5%8E%BB%E6%AD%BB%E4%B8%80%E5%9B%9E%E6%80%8E%E9%BA%BC%E6%A8%A3%EF%BC%9F.txt)
- [第21話　又以醉酒的勢頭幹壞事了](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00210_%E7%AC%AC21%E8%A9%B1%E3%80%80%E5%8F%88%E4%BB%A5%E9%86%89%E9%85%92%E7%9A%84%E5%8B%A2%E9%A0%AD%E5%B9%B9%E5%A3%9E%E4%BA%8B%E4%BA%86.txt)
- [第22話　看到她＜窸窸窣窣＞地撫摸自己](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00220_%E7%AC%AC22%E8%A9%B1%E3%80%80%E7%9C%8B%E5%88%B0%E5%A5%B9%EF%BC%9C%E7%AA%B8%E7%AA%B8%E7%AA%A3%E7%AA%A3%EF%BC%9E%E5%9C%B0%E6%92%AB%E6%91%B8%E8%87%AA%E5%B7%B1.txt)
- [第23話　有點恐怖啊](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00230_%E7%AC%AC23%E8%A9%B1%E3%80%80%E6%9C%89%E9%BB%9E%E6%81%90%E6%80%96%E5%95%8A.txt)
- [第24話　要變成不行的人了？](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00240_%E7%AC%AC24%E8%A9%B1%E3%80%80%E8%A6%81%E8%AE%8A%E6%88%90%E4%B8%8D%E8%A1%8C%E7%9A%84%E4%BA%BA%E4%BA%86%EF%BC%9F.txt)
- [第25話　無論如何都想要得到](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00250_%E7%AC%AC25%E8%A9%B1%E3%80%80%E7%84%A1%E8%AB%96%E5%A6%82%E4%BD%95%E9%83%BD%E6%83%B3%E8%A6%81%E5%BE%97%E5%88%B0.txt)
- [第26話　果然這種指示請饒了我吧](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00260_%E7%AC%AC26%E8%A9%B1%E3%80%80%E6%9E%9C%E7%84%B6%E9%80%99%E7%A8%AE%E6%8C%87%E7%A4%BA%E8%AB%8B%E9%A5%92%E4%BA%86%E6%88%91%E5%90%A7.txt)
- [第27話　即使獨自一人也要去尋找殿下](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00270_%E7%AC%AC27%E8%A9%B1%E3%80%80%E5%8D%B3%E4%BD%BF%E7%8D%A8%E8%87%AA%E4%B8%80%E4%BA%BA%E4%B9%9F%E8%A6%81%E5%8E%BB%E5%B0%8B%E6%89%BE%E6%AE%BF%E4%B8%8B.txt)
- [第28話　不論是任何困難，嘶～哈～，都能克服](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00280_%E7%AC%AC28%E8%A9%B1%E3%80%80%E4%B8%8D%E8%AB%96%E6%98%AF%E4%BB%BB%E4%BD%95%E5%9B%B0%E9%9B%A3%EF%BC%8C%E5%98%B6%EF%BD%9E%E5%93%88%EF%BD%9E%EF%BC%8C%E9%83%BD%E8%83%BD%E5%85%8B%E6%9C%8D.txt)
- [第29話　啊～啊～柯露雪大人](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00290_%E7%AC%AC29%E8%A9%B1%E3%80%80%E5%95%8A%EF%BD%9E%E5%95%8A%EF%BD%9E%E6%9F%AF%E9%9C%B2%E9%9B%AA%E5%A4%A7%E4%BA%BA.txt)
- [第30話　一直都很喜歡你](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00300_%E7%AC%AC30%E8%A9%B1%E3%80%80%E4%B8%80%E7%9B%B4%E9%83%BD%E5%BE%88%E5%96%9C%E6%AD%A1%E4%BD%A0.txt)


## [第三章](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0)

- [第１話　部分女生很熱情地支持著呢](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00010_%E7%AC%AC%EF%BC%91%E8%A9%B1%E3%80%80%E9%83%A8%E5%88%86%E5%A5%B3%E7%94%9F%E5%BE%88%E7%86%B1%E6%83%85%E5%9C%B0%E6%94%AF%E6%8C%81%E8%91%97%E5%91%A2.txt)
- [第２話　這麼說起來好像也是呢](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00020_%E7%AC%AC%EF%BC%92%E8%A9%B1%E3%80%80%E9%80%99%E9%BA%BC%E8%AA%AA%E8%B5%B7%E4%BE%86%E5%A5%BD%E5%83%8F%E4%B9%9F%E6%98%AF%E5%91%A2.txt)
- [第３話　想成為你的女人](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00030_%E7%AC%AC%EF%BC%93%E8%A9%B1%E3%80%80%E6%83%B3%E6%88%90%E7%82%BA%E4%BD%A0%E7%9A%84%E5%A5%B3%E4%BA%BA.txt)
- [第４話　比較那個（腦子不好使）的孩子啊](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00040_%E7%AC%AC%EF%BC%94%E8%A9%B1%E3%80%80%E6%AF%94%E8%BC%83%E9%82%A3%E5%80%8B%EF%BC%88%E8%85%A6%E5%AD%90%E4%B8%8D%E5%A5%BD%E4%BD%BF%EF%BC%89%E7%9A%84%E5%AD%A9%E5%AD%90%E5%95%8A.txt)
- [第５話　英雄開後宮，後宮成就英雄](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00050_%E7%AC%AC%EF%BC%95%E8%A9%B1%E3%80%80%E8%8B%B1%E9%9B%84%E9%96%8B%E5%BE%8C%E5%AE%AE%EF%BC%8C%E5%BE%8C%E5%AE%AE%E6%88%90%E5%B0%B1%E8%8B%B1%E9%9B%84.txt)
- [第６話　以……覆蓋上](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00060_%E7%AC%AC%EF%BC%96%E8%A9%B1%E3%80%80%E4%BB%A5%E2%80%A6%E2%80%A6%E8%A6%86%E8%93%8B%E4%B8%8A.txt)
- [第７話　明顯用下流的目光黏舔著](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00070_%E7%AC%AC%EF%BC%97%E8%A9%B1%E3%80%80%E6%98%8E%E9%A1%AF%E7%94%A8%E4%B8%8B%E6%B5%81%E7%9A%84%E7%9B%AE%E5%85%89%E9%BB%8F%E8%88%94%E8%91%97.txt)
- [第８話　不是當天便尋求發生肉體關係嗎？](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00080_%E7%AC%AC%EF%BC%98%E8%A9%B1%E3%80%80%E4%B8%8D%E6%98%AF%E7%95%B6%E5%A4%A9%E4%BE%BF%E5%B0%8B%E6%B1%82%E7%99%BC%E7%94%9F%E8%82%89%E9%AB%94%E9%97%9C%E4%BF%82%E5%97%8E%EF%BC%9F.txt)
- [第９話　回頭再好好收拾他](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00090_%E7%AC%AC%EF%BC%99%E8%A9%B1%E3%80%80%E5%9B%9E%E9%A0%AD%E5%86%8D%E5%A5%BD%E5%A5%BD%E6%94%B6%E6%8B%BE%E4%BB%96.txt)
- [第10話　那句台詞剛才也聽到過](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00100_%E7%AC%AC10%E8%A9%B1%E3%80%80%E9%82%A3%E5%8F%A5%E5%8F%B0%E8%A9%9E%E5%89%9B%E6%89%8D%E4%B9%9F%E8%81%BD%E5%88%B0%E9%81%8E.txt)
- [第11話　甚至對這樣的幼女也出手了嗎](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00110_%E7%AC%AC11%E8%A9%B1%E3%80%80%E7%94%9A%E8%87%B3%E5%B0%8D%E9%80%99%E6%A8%A3%E7%9A%84%E5%B9%BC%E5%A5%B3%E4%B9%9F%E5%87%BA%E6%89%8B%E4%BA%86%E5%97%8E.txt)
- [第12話　那是Flag台詞來的啊](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00120_%E7%AC%AC12%E8%A9%B1%E3%80%80%E9%82%A3%E6%98%AFFlag%E5%8F%B0%E8%A9%9E%E4%BE%86%E7%9A%84%E5%95%8A.txt)
- [第13話　那個只喝上一口就停不下來了](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00130_%E7%AC%AC13%E8%A9%B1%E3%80%80%E9%82%A3%E5%80%8B%E5%8F%AA%E5%96%9D%E4%B8%8A%E4%B8%80%E5%8F%A3%E5%B0%B1%E5%81%9C%E4%B8%8D%E4%B8%8B%E4%BE%86%E4%BA%86.txt)
- [第14話　聽說對……也有需求](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00140_%E7%AC%AC14%E8%A9%B1%E3%80%80%E8%81%BD%E8%AA%AA%E5%B0%8D%E2%80%A6%E2%80%A6%E4%B9%9F%E6%9C%89%E9%9C%80%E6%B1%82.txt)
- [第15話　只是拒絕人類的男性而已](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00150_%E7%AC%AC15%E8%A9%B1%E3%80%80%E5%8F%AA%E6%98%AF%E6%8B%92%E7%B5%95%E4%BA%BA%E9%A1%9E%E7%9A%84%E7%94%B7%E6%80%A7%E8%80%8C%E5%B7%B2.txt)
- [第16話　絕對不喝蜂蜜酒](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00160_%E7%AC%AC16%E8%A9%B1%E3%80%80%E7%B5%95%E5%B0%8D%E4%B8%8D%E5%96%9D%E8%9C%82%E8%9C%9C%E9%85%92.txt)
- [第17話　無視的話會不好吧？](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00170_%E7%AC%AC17%E8%A9%B1%E3%80%80%E7%84%A1%E8%A6%96%E7%9A%84%E8%A9%B1%E6%9C%83%E4%B8%8D%E5%A5%BD%E5%90%A7%EF%BC%9F.txt)
- [第18話　我認為這個隊伍名真是太棒了](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00180_%E7%AC%AC18%E8%A9%B1%E3%80%80%E6%88%91%E8%AA%8D%E7%82%BA%E9%80%99%E5%80%8B%E9%9A%8A%E4%BC%8D%E5%90%8D%E7%9C%9F%E6%98%AF%E5%A4%AA%E6%A3%92%E4%BA%86.txt)
- [第19話　難道說是我的錯？](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00190_%E7%AC%AC19%E8%A9%B1%E3%80%80%E9%9B%A3%E9%81%93%E8%AA%AA%E6%98%AF%E6%88%91%E7%9A%84%E9%8C%AF%EF%BC%9F.txt)
- [第20話　反正是死在野外了吧](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00200_%E7%AC%AC20%E8%A9%B1%E3%80%80%E5%8F%8D%E6%AD%A3%E6%98%AF%E6%AD%BB%E5%9C%A8%E9%87%8E%E5%A4%96%E4%BA%86%E5%90%A7.txt)
- [第21話　果然是難喝極了啊](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00210_%E7%AC%AC21%E8%A9%B1%E3%80%80%E6%9E%9C%E7%84%B6%E6%98%AF%E9%9B%A3%E5%96%9D%E6%A5%B5%E4%BA%86%E5%95%8A.txt)
- [第22話　有點不對勁](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00220_%E7%AC%AC22%E8%A9%B1%E3%80%80%E6%9C%89%E9%BB%9E%E4%B8%8D%E5%B0%8D%E5%8B%81.txt)
- [第23話　這下他們肯定睡著了](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00230_%E7%AC%AC23%E8%A9%B1%E3%80%80%E9%80%99%E4%B8%8B%E4%BB%96%E5%80%91%E8%82%AF%E5%AE%9A%E7%9D%A1%E8%91%97%E4%BA%86.txt)
- [第24話　他就是盧卡斯……？](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00240_%E7%AC%AC24%E8%A9%B1%E3%80%80%E4%BB%96%E5%B0%B1%E6%98%AF%E7%9B%A7%E5%8D%A1%E6%96%AF%E2%80%A6%E2%80%A6%EF%BC%9F.txt)
- [第25話　他們比豬頭人更不像話啊](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00250_%E7%AC%AC25%E8%A9%B1%E3%80%80%E4%BB%96%E5%80%91%E6%AF%94%E8%B1%AC%E9%A0%AD%E4%BA%BA%E6%9B%B4%E4%B8%8D%E5%83%8F%E8%A9%B1%E5%95%8A.txt)
- [第26話　我拉莫，殺人類](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00260_%E7%AC%AC26%E8%A9%B1%E3%80%80%E6%88%91%E6%8B%89%E8%8E%AB%EF%BC%8C%E6%AE%BA%E4%BA%BA%E9%A1%9E.txt)
- [第27話　能再飛高一點嗎？](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00270_%E7%AC%AC27%E8%A9%B1%E3%80%80%E8%83%BD%E5%86%8D%E9%A3%9B%E9%AB%98%E4%B8%80%E9%BB%9E%E5%97%8E%EF%BC%9F.txt)
- [第28話　把全員都收為眷姫吧](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00280_%E7%AC%AC28%E8%A9%B1%E3%80%80%E6%8A%8A%E5%85%A8%E5%93%A1%E9%83%BD%E6%94%B6%E7%82%BA%E7%9C%B7%E5%A7%AB%E5%90%A7.txt)
- [第29話　但是如果女士拿到手的話，就會變成百合了](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00290_%E7%AC%AC29%E8%A9%B1%E3%80%80%E4%BD%86%E6%98%AF%E5%A6%82%E6%9E%9C%E5%A5%B3%E5%A3%AB%E6%8B%BF%E5%88%B0%E6%89%8B%E7%9A%84%E8%A9%B1%EF%BC%8C%E5%B0%B1%E6%9C%83%E8%AE%8A%E6%88%90%E7%99%BE%E5%90%88%E4%BA%86.txt)
- [第30話　別那麼抬舉我啊](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00300_%E7%AC%AC30%E8%A9%B1%E3%80%80%E5%88%A5%E9%82%A3%E9%BA%BC%E6%8A%AC%E8%88%89%E6%88%91%E5%95%8A.txt)
- [第31話　真笨啊](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00310_%E7%AC%AC31%E8%A9%B1%E3%80%80%E7%9C%9F%E7%AC%A8%E5%95%8A.txt)
- [第32話　我也是那其中一員](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00320_%E7%AC%AC32%E8%A9%B1%E3%80%80%E6%88%91%E4%B9%9F%E6%98%AF%E9%82%A3%E5%85%B6%E4%B8%AD%E4%B8%80%E5%93%A1.txt)


## [第四章](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0)

- [第１話　是十多歲的嬌嫩身體哦](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00010_%E7%AC%AC%EF%BC%91%E8%A9%B1%E3%80%80%E6%98%AF%E5%8D%81%E5%A4%9A%E6%AD%B2%E7%9A%84%E5%AC%8C%E5%AB%A9%E8%BA%AB%E9%AB%94%E5%93%A6.txt)
- [第２話　以那麼多人為對象，獨自對付得了嗎](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00020_%E7%AC%AC%EF%BC%92%E8%A9%B1%E3%80%80%E4%BB%A5%E9%82%A3%E9%BA%BC%E5%A4%9A%E4%BA%BA%E7%82%BA%E5%B0%8D%E8%B1%A1%EF%BC%8C%E7%8D%A8%E8%87%AA%E5%B0%8D%E4%BB%98%E5%BE%97%E4%BA%86%E5%97%8E.txt)
- [第３話　竟敢對我的女兒出手](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00030_%E7%AC%AC%EF%BC%93%E8%A9%B1%E3%80%80%E7%AB%9F%E6%95%A2%E5%B0%8D%E6%88%91%E7%9A%84%E5%A5%B3%E5%85%92%E5%87%BA%E6%89%8B.txt)
- [第４話　在角落好好作為空氣](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00040_%E7%AC%AC%EF%BC%94%E8%A9%B1%E3%80%80%E5%9C%A8%E8%A7%92%E8%90%BD%E5%A5%BD%E5%A5%BD%E4%BD%9C%E7%82%BA%E7%A9%BA%E6%B0%A3.txt)
- [第５話　這不是特大的自找麻煩嗎？](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00050_%E7%AC%AC%EF%BC%95%E8%A9%B1%E3%80%80%E9%80%99%E4%B8%8D%E6%98%AF%E7%89%B9%E5%A4%A7%E7%9A%84%E8%87%AA%E6%89%BE%E9%BA%BB%E7%85%A9%E5%97%8E%EF%BC%9F.txt)
- [第６話　不知為何發出尖銳的響聲](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00060_%E7%AC%AC%EF%BC%96%E8%A9%B1%E3%80%80%E4%B8%8D%E7%9F%A5%E7%82%BA%E4%BD%95%E7%99%BC%E5%87%BA%E5%B0%96%E9%8A%B3%E7%9A%84%E9%9F%BF%E8%81%B2.txt)
- [第７話　剛抓到很新鮮](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00070_%E7%AC%AC%EF%BC%97%E8%A9%B1%E3%80%80%E5%89%9B%E6%8A%93%E5%88%B0%E5%BE%88%E6%96%B0%E9%AE%AE.txt)
- [第８話　（期待）百合（ﾟ∀ﾟ）！！](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00080_%E7%AC%AC%EF%BC%98%E8%A9%B1%E3%80%80%EF%BC%88%E6%9C%9F%E5%BE%85%EF%BC%89%E7%99%BE%E5%90%88%EF%BC%88%EF%BE%9F%E2%88%80%EF%BE%9F%EF%BC%89%EF%BC%81%EF%BC%81.txt)
- [第９話　能稍微看一下周圍的氣氛便謝天謝地了](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00090_%E7%AC%AC%EF%BC%99%E8%A9%B1%E3%80%80%E8%83%BD%E7%A8%8D%E5%BE%AE%E7%9C%8B%E4%B8%80%E4%B8%8B%E5%91%A8%E5%9C%8D%E7%9A%84%E6%B0%A3%E6%B0%9B%E4%BE%BF%E8%AC%9D%E5%A4%A9%E8%AC%9D%E5%9C%B0%E4%BA%86.txt)
- [第10話　看來此子乳量相當驚人](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00100_%E7%AC%AC10%E8%A9%B1%E3%80%80%E7%9C%8B%E4%BE%86%E6%AD%A4%E5%AD%90%E4%B9%B3%E9%87%8F%E7%9B%B8%E7%95%B6%E9%A9%9A%E4%BA%BA.txt)
- [第11話　超級軟綿綿的](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00110_%E7%AC%AC11%E8%A9%B1%E3%80%80%E8%B6%85%E7%B4%9A%E8%BB%9F%E7%B6%BF%E7%B6%BF%E7%9A%84.txt)
- [第12話　對這樣的我說愛是很肚子疼的](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00120_%E7%AC%AC12%E8%A9%B1%E3%80%80%E5%B0%8D%E9%80%99%E6%A8%A3%E7%9A%84%E6%88%91%E8%AA%AA%E6%84%9B%E6%98%AF%E5%BE%88%E8%82%9A%E5%AD%90%E7%96%BC%E7%9A%84.txt)


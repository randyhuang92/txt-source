熟悉古柯山的凱恩幾乎一條直線地朝哥布林的巢穴走去。

「我還以為是這裏，剛才的爆炸到底是什麼？」

難道敵人裏有魔法師嗎？
雖然也聽說過極少有哥布林魔法師的這一種可以使用魔法的哥布林，但是那樣的大魔法是不可能使用的。
凱恩小心翼翼地走進原是巢穴的地方，他認為偵查的任務必須做的盡善盡美。
可是不管往哪裏看，都只有哥布林的焦黑屍體⋯

「互相殘殺？」

不⋯ 不會有那種事吧。
就在這時，傳來一陣聲響，有什麼東西倒在了凱恩的身邊。

「哇！」

凱恩忍不住揮舞著秘銀之劍，突然刺中了哥布林王的頭。
這也許就是遭受了比死了更好的劇痛的哥布林王的自殺。
就這樣，看到了很大的火柱，急忙趕了過去的討伐隊的主力部隊正好來了。

「嘿，這不是哥布林王嗎？」

「是凱恩先生做的？！「

驚恐的討伐隊的冒険者們。
特別是，在事情發生後第一個進入的Ａ級隊伍『追逐流星之人』的成員，親眼目睹了凱恩刺穿哥布林王的場面。

『啊，阿貝爾該怎麼辦呢？這已經不是殺哥布林領主的時候的事了。』

女盜賊綺莎菈戲弄阿貝爾。
陷入沈默的阿貝爾走到刺著哥布林王的正發呆的凱恩面前，深深地低下了頭。

『大叔，不，凱恩先生，對不起。讓我向你道歉，讓我向你輕視Ｄ等級的行為道歉。』
『嗯，啊，那倒也沒什麼⋯⋯』

青髮劍士阿貝爾出乎意料地率真。
雖然很年輕，但他是上升到Ａ級的英雄。
雖然他還不到二十歳，還對凱恩說了些乖僻的話。
只要在自己的眼前展現出對方的實力，就有承認對方是勇士的才能。

「這樣啊，你能原諒我嗎？從現在開始，凱恩先生是我的對手！」

剛承認了實力，這次就認定了凱恩是競爭對手的阿貝爾。
幹脆是很好，不過，還是麻煩的人。

「不，我什麼都沒做。」
「我看到了凱恩先生打倒了哥布林王。對我來說也是個不知道能不能一人打倒的怪物，太過謙遜是令人討厭的。」
「不，你看，哥布林好像都被魔法燒毀了，但他不是魔法使，所以不能使用火焰球和火球的魔法。」

回答此問題的是正在調查成為消炭的哥布林的黑髮女魔法師庫爾茨，

戴著瓶底眼鏡，身材嬌小卻背著很多魔導書走路的樸素的她。
雖然看起來是這樣，但還是艾倫街上最強的Ａ級隊伍「追逐流星之人」的成員。
她是一位從王国魔法學校畢業的優秀Ａ級女魔法師。

「這不是火球的火力。如果不是傳說中的大規模魔法、獄炎殲滅盡的話，就不會達到這種程度」
「庫爾茨雖有在學習但可真傻啊。凱恩是這麼說的，對吧？」
「不，這是最初級的魔法，不會的。」
「凱恩先生若說的是火焰球，所以那就是火焰球。」

被女盜賊綺莎菈這麼說了好幾次，女魔法師庫爾茨露出恍然大悟的表情。

「這麼說來，我好像在哪裏讀過。請等一下。啊，有了！」

戴著眼鏡的庫爾茨拿出自己背的一本書，一邊翻一邊朗讀。

「奧斯托利亞王国英雄列傳，第兩百三十八頁《傳說的大賢者達納・琳》的章節！豬人領主率領的三百隻的魔物的王都入侵，大賢者達納即一瞬間將之燒光怠盡。一旁的魔術師問『怎麼會，達納導師用獄火滅光了？而且還是獄炎殲滅盡的無詠唱？』大賢者回：『這是火焰球啦！』⋯」

接下來四周一片死寂，雖然誰都沒說，但心裏全都只有一個念頭。
凱恩擁有與傳說中的大賢者達納・琳同等的魔力！？
不不不，你們到底在講什麼，凱恩呆住了。

凱恩重新振作起來，向大家說明。

「所以說我來的時候全都被打敗了。」
「沒錯，沒錯⋯⋯凱恩先生沒有做。沒錯，大家都明白了！」

巨漢蘭德爾拍著手說。也就是說，這件事是要保密的，大家互相看了看，微妙地點點頭。

「不，所以我真的沒做！」
「嗯，凱恩先生，既然已經圓滿結束了，不就好了嗎？」

雖然情況完全沒有收斂，但也只能這麼做了⋯

順便一提，哥布林的巢穴討伐的委託金，那個權利的大部分都交給了凱恩，但是可不能收取連打倒都沒有的魔物的錢。

而且這次的委託金大部分還是由貧困的古柯村出的費用。
因此，凱恩決定用於古柯村不足的食物和日用品等作為支援物資，匿名提供給受災的村莊。
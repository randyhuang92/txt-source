 南方探索命令

「事情就是這樣，令公子為了保護赫爾姆特王國的貴重資產琳蓋亞號以及眾多船員，才主動選擇了讓自己被收監的道路。普拉特伯爵！他是貴族的榜樣呢！」

「是這樣啊……」

「哎呀，我鮑麥斯特伯爵。真是對令公子的決斷打心底覺得感動呀。閣下的公子真是貴族的表率」

「如果知道鮑麥斯特伯爵對自己的評價高到這個地步，犬子一定會非常高興吧」

「（呵呵呵，生氣了。生氣了）」

將虛偽的報告告訴給在王城內遇到的普拉特伯爵時，他只能一邊臉抽筋的一邊聽我嘮叨。

當然，我的話完全是和事實一點不符的謊言。

目送被解放的琳蓋亞號平安出航後，我先一步用『瞬間移動』飛往王城向陛下說明了事情經過。

不管怎麼遮掩，普拉特伯爵的笨蛋兒子先搞出攻擊的事實都無法覆蓋。

佐奴塔克共和國政府現在很混亂，此時如果再去和他們糾扯國家尊嚴什麼的話，琳蓋亞號和其船員可能就很長時間內都回不來了。

於是，我想辦法讓魔族之國政府快速判決了這件事的執行犯快速安納金再緩期執行並上繳罰金，因為這個錢大部分是由我墊付的，所以安納金為了還錢決定去我家任職。

最後身為主犯的普拉特伯爵的笨蛋兒子擔下一切罪名被關進了魔族之國刑務所，至於琳蓋亞號和其他船員則已經平安向這邊返航。

「這就是我能做到的極限了」

「既然琳蓋亞號和貴重的船員們能夠回來。那麼雖然說不上最佳但也可是算是很好的結果了吧」

魔族之國也為擅自扣押琳蓋亞號及其船員這件事道了歉，並立刻將兩者解放。

王國也不用賠償什麼。

雖然也有貴族……也就是普拉特伯爵主張讓對方賠償王國，但那種條件魔族不可能接受。

而且他還只能自己不站出來只在背地裏碎碎念，不然就會激怒一直在外交交涉上沒什麼進展的尤巴夏外務卿說出『那麼，你就自己去直接交涉吧！』的話來。

我如果是魔族那邊的人也不可能接受那，所以才選擇了把一切罪名都推給普拉特伯爵的笨蛋兒子這個最簡單的方法。

突然想到。

好像是某個創作作品中，說過國家間進行交涉的時候不管是大蟲還是小蟲都會很難生存。

所以說我不擅長交涉嘛。

『那種人連小蟲的價值都沒有』

『也沒有一點貴族風範呢』

『反正就算救他，那傢伙也不會感謝你。不如說，他還會抱怨威德林動作太慢呢。活該被捨棄』

可以說是理所當然的吧，維爾瑪、卡特莉娜，泰蕾莎都將笨蛋兒子評價為最差勁的傢伙。

埃莉絲則此什麼也沒說。

『感覺就很糟糕的人呢』

『艾茉莉你一句話就把那傢伙完全形容出來了吶』

的確，那傢伙是個一點救都沒有的真正的笨蛋。

如果他還有點智商的話，被關進牢房起就該想到即使一點點也好得儘快給人留些好印象。

因為是大貴族的繼承人，所以就不擅長忍耐和自律了嗎。

反正主犯的確是他。

被冤枉很可憐，但這也是他自作自受，所以我一點幫助他的想法也沒有。

不過僅僅是把他作為祭品交給魔族之國的話也會產生問題，所以就利用了下他是貴族這個事實。

說他是為了琳蓋亞號和其他船員們，才選擇只讓自己一人被收監的道路的。

當然了，那傢伙才沒有這麼高尚的人格，但這麼說的話起碼能保住他作為貴族的體面。

普拉特伯爵大概滿心都是想對我怒吼的心情吧，但就是無法如願。

先前告訴他笨蛋兒子今後的下場時，他只能嚷著『我的兒子是貴族的光榮！』這種話哭著笑給我看。

因為不對世間這麼說不行呢。

「鮑麥斯特伯爵也會做很惡辣的事吶」

「我好歹守護住了他的名譽」

雖然現在獨自逃回來會遭到嚴懲，但將來笨蛋兒子在刑務所裡服完刑後是可以返回王國的，另外他作為空軍也得到了不錯的評價。

名譽和實績都有了，那麼回來後應該能得到不需要負什麼責任的職務吧。

因為遠離實務我想他今後大概沒什麼機會再發出攻擊命了，應該能好好休息到退休為止。

先不說本人對這樣的待遇有什麼想法，至少在我看來這是個挺不錯的結局。

「說起來，以他的罪狀需要被收監多久呢？」

「二十五年到三十年吧」

命令手下攻擊國有的警備隊艦船。

做出這種事光是不判終生監禁或者死刑就該偷笑了

加害社會上層的行為，是超乎平成日本人想像的重罪。

如果有人在王國犯下類似的罪行，最糟的時候可能會被判死刑。

什麼？那安納金怎麼說？

那傢伙就只是執行了笨蛋的命令而已，已經靠司法交易作為特例釋放了。

「畢竟也沒有給對方造成實際損害，接下來就看他在監獄裡怎麼表現了。要是能成為模範囚犯的話刑期好像可以縮減到二十年左右」

「原來如此。既然琳蓋亞號和船員們能回來就沒問題了。那艘船回來後，這次朕打算派他們前往東方探索」

對於做出蠢事的普拉特伯爵的笨蛋兒子，陛下也在內心對他感到非常憤怒吧

所以很快就不再提他。

重要的是，運用琳蓋亞號進行的探索活動看來還要繼續。

這塊琳蓋亞大陸自從繁榮的古代魔法文明崩潰以來，幾乎沒人做過什麼對周邊地域的探索。

王國這邊，應該也想得到當人口達到飽和時可以用來移民的土地吧。

雖然現在琳蓋亞大陸還處於開發尚未結束的狀態，但作為支配一國的為政者大概必須得以長期視角來考慮問題才行。

這麼說來，對鮑麥斯特伯爵領南方群島以南區域的探索也一直沒能進行來著。

「現在光是應對魔族就竭盡全力了」

魔族中的大多數人都很理性。

雖說不是把『對人類國家發動侵攻———！把人類全殺光———！』掛在嘴上的種族真是得救了，但他們的潛在力量大的過分。

交涉必須儘可能自然而慎重的進行。

「實際上，帝國也送來了交涉團」

「彼得……不是，對方的陛下送交涉團過來的動作很遲緩呢」

「之前都在觀望吧。帝國這麼做對於我國說不定意外有利吶」

魔族也好王國也好帝國也好，都各有各的盤算。

想調整這三者之間的利害關係的話，沒有人知道到底需要交涉多久才會結束

「聽說在魔族之國裡，原本沒有進行外交工作的部門。是到了這次事件時才匆忙組建的？」

「因為他們已經超過一萬年以上沒有和外國交流了」

「政府雖然送來了交涉團交涉，所有成員卻都不擅長外交嗎，越來越感覺事情難以進展了」

魔族之國政府剛剛進行過政權交換。

所以才無法送有實力的務實者過來吧。

這方面的情報，王國似乎已經掌握到了。

我雖然利用官員們讓琳蓋亞號的解放交涉順利運行起來，但這些人如果在外交方面太出頭政治家們肯定要有意見。

如果他們不能把輔助的角色做到底，交涉無法進展的可能性就很高了。

「此外還有其他問題，既然要進行貿易，那就要牽扯到貨幣匯率了」

「匯率定得太惡劣的話，就會導致單方面財富外流了呢」

如果像日本幕末那樣金銀之間的兌換匯率差距太大的話，王國的國力一定會急速衰退。

太過焦急犯錯的話帝國也是同樣下場。

畢竟，王國和帝國使用的貨幣幾乎完全一樣。

「魔族之國，會出口過來優秀的魔道具吧」

「陛下，關於那件事……」

「魔道具工會似乎已經聽到風聲了。發生了很厲害的騷亂。」

不管怎麼看，王國和帝國的魔道具在技術力上都比不過魔族之國。

根據我的觀察，人類這邊的技術至少也落後好幾百年。

再加上魔族之國的魔道具在量產方面上也很優秀，所以價格還不是很貴。

所以如果魔族之國的魔道具大量流入王國，魔道具工會到時肯定會陷入關門停業狀態吧。

「魔道具工會發生騷亂這點上帝國那邊也是同樣情況。甚至連瑞穗公爵領那邊也是」


瑞穗公爵領的魔道具比王國和帝國都要優秀。

現在這一優勢即將崩潰，他們當然會慌張了。

「得靠施加關稅來限制進口量呢」

「確實有這種方法」

然而，不管是施加關稅，還是限制進口量，都得先得找出相關的具體數字資料才行。

再說了，對方有可能根本就不接受自由貿易的主張。

「帝國的皇帝似乎也感到很頭疼啊」

「應該是的」

接著內亂的機會，王國和帝國之間設置了最高層可以直接進行通話的魔導通信機。

也就是所謂的紅色專線。

兩位國家領袖，都在為如何應對魔族之國而煩惱。

異文化交流雖然說起來輕鬆，但國與國之間要是真能那麼簡單就達成友好關係，就不會發生那麼多戰爭了。

想得出雙方都能在一定程度接受的條件讓人類魔族開始交流，看來必須花費不得了的時間和勞力才行。

「暫時，就採取全交給尤巴夏外務卿的做法嗎」

「是的」

這次進行的交涉，終究只是臨時委派的。

所以我現在也不好對尤巴夏外務卿的工作說三道四。

不然搞不好又會惹上什麼麻煩事。

話雖如此，我也在短時間內連續當了兩次特使呢。

「那麼，臣就回領地去了」

「鮑麥斯特伯爵，你這次辛苦了。領收完經費和嘉獎你就可以回去了」

向陛下辭行過後，我使用『瞬間移動』飛回了鮑麥斯特伯爵領。

抵達府邸時，羅德里希立刻前來迎接我。

「當家大人，您受累了」

「沒事，來自上面的指派躲不掉嘛。比起那個，我完全和普拉特伯爵成了敵人哦」

普萊特伯爵一定恨透了把他的兒子出賣給外國官府的我，可他就算再怎麼想對我破口大罵也罵不了。

「這也是沒辦法的。以後我會多加注意普拉特伯爵和親近他的那些人的行動」

「果然，你也覺得以後得小心點他們嗎」

雖說能乾脆識別的敵對關係對應起來更容易，但我也不是沒有【即便是裝樣子也罷，果然還是和睦相處的關係好】的想法。

「是也有那種處事八面玲瓏的貴族，但家主大人既然成了超級大貴族，那麼有交惡的貴族也是沒辦法的事。而且，知道對方肯定是敵人我們這邊對應起來也輕鬆」

知道是敵人，所以就不必擔心被虛偽的好意或善意欺騙了嗎。

和普萊特伯爵要好的貴族也得注意呢。

「曾有比喻說，所謂貴族，就是笑呵呵的用你以為是他的慣用手來握手時，其實正用真正的慣用手握著匕首等著你的人」

「原來如此」

很形象的比喻。

面對貴族不能大意這件事我深有體會。

「話說回來，霍爾米亞邊境伯真是因為這次的動亂虧慘了」

因為交涉還得繼續，群島就被那麼魔族軍隊佔領了下去。

島上軍事基地的建設也有了些進展。

由於帝國也摻了進來，所以交涉現場轉移到了那個軍事基地的一角，但最關鍵的交涉本身還是沒什麼進展。

兩個國家交涉都那麼糾纏不清了，現在交涉方增加到三個國家情況當然會更加錯綜複雜。

而且，現在的帝國還處於必須小心提防瑞穗公爵家的狀態。

「要解除諸侯軍的動員令很困難啊」


「起碼完全解除是不可能了」

對於霍爾米亞邊境伯來說，以現在的狀況諸侯軍的動員令是不可能完全解除的。

現在駐紮正在前線的軍隊數量雖然大幅減少，但即便如此帶來負擔依舊很大。

畢竟，軍隊就算什麼也不做也會消耗費用。

「嘛，反正我等鮑麥斯特伯爵家唯一被動員的家主大人已經回來了呢，接下來就能安心傾注於領地內的開發了喲」

雖然和魔族有關的事件完全看不到解決的跡象，但我既不是國王陛下也不是外務卿。

只需要關心自己的領地就行了。

就這樣，我們又回歸了原本的生活。

「這座小山採取整座剷平再修理成平地的做法比較好吧？」

「的確。這裡整成平地的話建設宅基就容易多了」

鮑麥斯特伯爵領內未開發地很多。　

不如說幾乎都是未開發地，所以如果土木工程跟不上到人口增加時就讓人頭疼了。

因為移居條件好，所以這裡聚集了從王國來的諸如沒有土地的農家次男三男；從學徒身份畢業獨立卻苦於沒有客人工匠；沒有容身之處的貴族或陪臣子弟等人物。創造供這些人居住的宅基就是眼下的當務之急。

連和我一起使用魔法整備土地的卡特莉娜，現在也習慣了做這種事。

「老師，和魔族之國的交涉不必再進行了嗎？」

前往西部時，我沒有帶上向我學習魔法土木工程也會來幫忙的阿格妮絲她們。

這個世界和日本不同要是發生了什麼大事件無關係的人想得知事件的詳細經過得經過相當長時間才能辦到，所以對事件很好奇她們就向我打聽了魔族之國的事。

「那個啊，大概會糾纏下去吧？」

「誒誒———！這樣好嗎？」

「無所謂好不好吧？」

「也對呢」

國與國之間的外交交涉，沒人能保證最後一定會有什麼結果。

連日本那邊，北方領土什麼的也是花了幾十年的時間都還沒要回來呢。

「大部分魔族，都沒有侵略大陸的想法」

連自己居住的島都有很多因為人口減少而廢棄的場所，那麼就算攻克了大陸要維持住佔領也很難。

不過，那些大企業似乎有進軍規模是本國五十倍以上的大陸市場的想法。

說起來，那些人生產的產品如果流入大陸，王國的魔道具工會肯定會衰退。

那麼食品生產又如何呢？

魔族之國的食品生產量和有關技術都很不得了，所以多半就算進口也會因為價格太高導致只有一部分富裕階層能買得起吧。

不如說，王國的便宜食品說不定會反過來流入魔族之國。

雖然王國本身有食品不足的跡象，但商人們只會把食品賣給更有賺頭的一方。

魔族之國為了保護本國農業或者說是食品自給率，一定會對進口食品收關稅甚至是禁止進口食品，這麼一來王國也就能提出對魔道具的進口施加關稅甚至是禁止進口了。

「老師，這話題好難懂」

弟子三人中最年少的辛蒂皺著眉頭這麼說道。

「確實。其實做說明的我是最不明所以的」

「即便想要締結正式的交流條約，雙方卻各有內情呢」

「就是這麼回事」

雖然貝蒂說的比較婉轉，但簡單來說就是既得權益受到侵犯的反抗勢力很難纏。

王國這邊是魔道具工會強烈抗議。

要是包含壓倒性優秀技術力的魔族之國魔道具進口進來，他們的力量一定會大幅降低。

『有這邊的最新技術被假想敵國獨佔去了的危險』，他們似乎是用這種有點本末倒置的藉口來阻止魔道具進口的。

魔道具工會很有錢，為了運營工會還僱傭了很多貴族子弟。

因此若魔道具工會的銷售利潤降低就等於砍了這些人的腦袋。所以有很多貴族跑去對尤巴夏外務卿施加壓力。

魔族之國政府那邊肯定也受到了來自農業、畜業、漁業企業相關團體的壓力，這種環境下交涉是不可能順利進行的。

「問題之一，就在於群島現在還在魔族之國的佔領下」

『擅自佔領了王國領地的魔族不能信任！』說著這種話的貴族很多，

魔族之國那邊，似乎把提拉哈雷斯群島視為發動無謀攻擊的赫爾姆特王國作為賠償轉讓給他們的島，然而即便對方只有一部分人產生這種認識也是很不妙的。

提拉哈雷斯群島是霍爾米亞邊境伯領的領土，王國不能擅自拿來用在外交交涉上甚至轉讓出去。

而且王國已經為擅自攻擊的事賠了罪，也處罰了實行犯們。

明明事情已經了結了，可仍因此被人擅自奪走領地的話任誰都受不了吧。

雖然只是個沒有任何人在用的群島，但貴族和國家也是有自尊心的，不可能隨隨便便把自家領土讓給他國。

「就是說……」

「交涉會被拖得非常久，威爾你只需要專心考慮自家領地的事就行了」

「我一開始就沒有插手的意思……」

今天，埃裡希哥哥也和我們一起來到了工事現場。

為了讓鮑麥斯特伯爵領的開發順利進行，埃裡希哥哥由陛下親自任命為我的輔佐和聯絡官。

今天他就是為了視察而來的。

「威爾作為臨時特使去過魔族之國，也拿帶回了交涉成果。所以不必再多參與了呢。比起那些事，還是領地的開發更重要」

進行外交交涉期間，王國的統治和內政並不會就此停滯下來。

不如說，反而還得把【和魔族之國相比，我國時時刻刻都在發展】這件事好好宣傳出去才行。

「威爾你已經配合恩斯特殿下的情報提交了關於佐奴塔克共和國的報告吧？因為那邊是人口持續減少的社會，所以只要把咱們這邊一直在發展的事宣傳出去，就能帶給對方壓力了」

既然技術力和魔法使的數量完全不是對手，那就在能贏過的要素上面施加壓力。

這也算是一種戰爭吧。

「另外還有帝國在。那個國家雖然因內亂國力受到了巨大的傷害，但相對的中央的力量也變強了。從長遠來看今後一定會有巨大的成長吧」

迄今為止中央必須小心對待的選帝候家族大多已經沒落，新皇帝的彼得既年輕又有能力。

以復興因內亂荒廢的帝國為名義的大規模開發也一個接一個的開始了，疏忽大意的話甚至產生王國的國力被帝國超過去的危險。

「我覺得短時間內我們和帝國不會交惡，那期間王國也必須積蓄自己的力量才行。與魔族之國的交涉由於帝國也加入進來越發複雜化了。正好可以趁機爭取時間」

就是說交涉停滯、大量耗費時間這些情況，不如說反倒對王國有利嗎。

「帝國的交涉團，應該也會對魔族之國的說法感到莫名其妙吧」

聽到『野生動物很可憐所以不要再狩獵它們了』這種話，帝國肯定也會產生混亂。

「所以呢，咱們是咱們，別人是別人的結論就出來了呢。因為我是財務派的法衣貴族，威爾的領地變繁榮的話我就能得到間接增加王國稅收的評價哦。要是王國政府和鮑麥斯特伯爵家關係良好的話這個評價就更上一層樓了」

埃裡希哥哥是財務系的貴族，所以他的標準簡單來說就是錢最優先。

沒錢等於掉腦袋，這種事在哪個世界都一樣。

畢竟我們兩個都因為沒錢的老家受過很多罪。

「所以，才來這裡促進開發嗎？」

「也有這方面的理由，不過其實我給威爾你帶來了一個王國的委託」

「委託嗎？」

「是的，鮑邁斯特伯爵家領地的南方有群島的吧？」

「是啊……」

那是距離南邊海岸沒多遠的，我自己發現的島嶼，王國已經把那裡當做鮑麥斯特伯爵領領土看待了。

因為其中有很多生長著大量野生甘蔗的島，現在有數百人在那裡進行著栽培甘蔗和製糖業的經營。

漁業也很興盛港口設施也準備好了，所以人口正在慢慢增加。

「這次就是想弄清楚那個群島更南方地域的情況」

「所以，探索用的大型魔導船才是必須的？」

向西方探索時，用上了琳蓋亞號級別的船。

我家運用的魔導飛行船，是無法進行距離那麼遙遠的探索的。

「如果進行那種大型探索的話，王國就必須派出大型艦船了喲。這次的探索範圍最多只有數百公里。是在鮑麥斯特伯爵家魔導飛行船的行動範圍之內的探索呢。畢竟有必要掌握清楚鮑麥斯特伯爵領領土的整體狀況嘛」

就是說如果是不使用大型船就找不到的新領地，因為船由王國來出因此所有權也就另算了嗎。

「新領地的探索啊……」

「向西方探索後發現了魔族。北方那邊聽說帝國已經預定要派出探索隊。東邊這邊也一樣，王國已經指定相關計畫了」

如果不能先於帝國發現就無法確保新領地的擁有權，所以先不管其他有的沒的儘早把探索隊派出去再說嗎。

這件事落到南方方向時，就成了確認鮑麥斯特伯爵領的作業。

「明白了。那我就去和羅德里希說的開始準備船吧」

「我也會同行」

和魔族的交涉雖然完全沒有進展，但鮑麥斯特伯爵領的開發可不持續推進不行。

所以才必須對領地進行確認作業。於是我向羅德里希下達了組建南方探索隊的命令。
